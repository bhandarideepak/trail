﻿using Microsoft.AspNetCore.Mvc;
using NUnit.Framework;
using System.Net;
using WelcomeApp.Controllers;

namespace WelcomeApp.UnitTests
{
    public class GreetingsControllerTest
    {
        private GreetingsController sut;

        [SetUp]
        public void BeforeEach()
        {
            sut = new GreetingsController();
        }


        [TestCase("John")]
        public void Get_ShouldReturnWelcomeMessage_WhenNameHasValue(string name)
        {
            var greetMessage = $@"{{ message = Welcome to BBC Studios {name} }}";
            var result = sut.Get(name) as OkObjectResult;
            Assert.AreEqual((int)HttpStatusCode.OK, result.StatusCode);
            Assert.AreEqual(result.Value.ToString(), greetMessage);
        }
      
    }
}
