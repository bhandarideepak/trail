﻿using Microsoft.AspNetCore.Mvc;

namespace WelcomeApp.Controllers
{
    [Route("greetings")]
    [ApiController]
    public class GreetingsController : ControllerBase
    {
        /// <summary>
        /// Method provide welcome message to user
        /// </summary>
        /// <param name="name">Name of the person</param>
        /// <returns>Greeting message with person name.</returns>
        [HttpGet("{name}")]
        public ActionResult Get([FromRoute]string name)
        {
            return Ok(new { message = $"Welcome to BBC Studios {name}"});
        }
    }
}
