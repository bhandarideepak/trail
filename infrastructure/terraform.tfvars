/*
 * These variables will be consumed automatically by terraform and 
 * provide default settings for local development. They can also be 
 * overridden by other variable files passed to terraform.
 */

environment = "dev"
application = "bbc-studios-trail"
app_version = "0.1.0.0"
location    = "North Europe"
area_prefix= "bbc_poc"